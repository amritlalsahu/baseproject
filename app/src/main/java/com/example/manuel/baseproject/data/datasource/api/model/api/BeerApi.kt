package com.example.manuel.baseproject.data.datasource.api.model.api

data class BeerApi(
        val id: Int? = null,
        val name: String? = null,
        val tagline: String? = null,
        val image: String? = null,
        val abv: Double? = null
)