package com.example.manuel.baseproject.home.beers.domain.model

enum class AbvRangeType {
    HIGH,
    NORMAL,
    LOW
}