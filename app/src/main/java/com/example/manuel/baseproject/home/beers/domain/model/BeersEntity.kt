package com.example.manuel.baseproject.home.beers.domain.model

data class BeersEntity(
        val beers: List<BeerEntity>
)