package com.example.manuel.baseproject.home.beers.vm.model

enum class AbvColorType {
    GREEN,
    ORANGE,
    RED
}